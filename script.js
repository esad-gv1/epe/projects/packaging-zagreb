import { loadScripts } from "./js/utils.js";
import { paginate } from "./js/paginate.js";

const elementsToPaginate = [];

//CSS used in this project, including the pagedjs preview css
const paginationStyleList = [
    "css/printWindow.css",
    "vendors/css/paged-preview.css"
];

//additional script to load, not importable as es modules
const scritpList = [
    "vendors/js/markdown-it.js",
    "vendors/js/markdown-it-footnote.js",
    "vendors/js/markdown-it-attrs.js",
    "vendors/js/markdown-it-container.js",
    "vendors/js/markdown-it-span.js",
];

//sync batch loading
await loadScripts(scritpList);

//markdown files to load
const mdFilesList = [
    "md/chapitres.md",
    "md/chapitre1.md",
    "md/chapitre2.md",
    "md/chapitre3.md",
    "md/chapitre4.md"];

//markdownit instanciation (old school method as no ES6 modules are available)
const markdownit = window.markdownit
    ({
        // Options for markdownit
        langPrefix: 'language-en',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer) //div
    .use(markdownitSpan) //span
    .use(markdownItAttrs, { //custom html element attributes
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });

//function to produce the HTML from md files
async function layoutHTML() {

    const chapitres = [...document.getElementsByClassName("chapitre")];
    const contents = [...document.getElementsByClassName("content")];
    //Handles close and print bt separately
    const closeBt = document.getElementById("closeBt");
    // const printBt = document.getElementById("printBt");
    const tocImg = document.getElementById("toc-img");

    //!important! forEach can't be used as it doesn't respect await order!
    for (let index = 0; index < mdFilesList.length; index++) {

        const mdFile = mdFilesList[index];
        console.log("mdFile", mdFile);
        const reponse = await fetch(mdFile);
        const mdContent = await reponse.text();
        //convertion from md to html, returns a string
        const result = markdownit.render(mdContent);

        if (mdFile.includes("chapitres")) {
            const documentFromString = new DOMParser().parseFromString(result, "text/html");
            const divs = documentFromString.body.getElementsByTagName("div");

            chapitres.forEach((chapitre, index) => {
                chapitre.innerHTML = divs[index].innerHTML;
                //prepare interactions
                chapitre.addEventListener("click", onInteractChapiter);
                chapitre.addEventListener("mouseenter", onInteractChapiter);
                chapitre.addEventListener("mouseleave", onInteractChapiter);
            });
        }
        else {
            const contentElement = document.getElementById("content" + index);
            //console.log("contentElement", contentElement);
            contentElement.innerHTML = result;
        }
    };

    //handles content interactions
    contents.forEach((content) => {
        const contentChildren = [...content.children];
        //console.log(contentChildren);
        contentChildren.forEach((element) => {
            //console.log(element.tagName);
            const elementName = element.tagName.toLowerCase();
            if (elementName !== "h1" && elementName !== "h2") {
                element.addEventListener("click", onInteractContent);
            }
        });
    });

    //closebt
    closeBt.addEventListener("click", (event) => {

        const chapitres = [...document.getElementsByClassName("chapitres")];
        const contents = [...document.getElementsByClassName("content")];

        chapitres.forEach((chapitre) => {
            chapitre.style.display = "none";
        });

        contents.forEach((content) => {
            content.style.display = "none";
        });

        tocImg.style.display = "none";
    });

    window.addEventListener("resize", onWindowResize);
}

function onInteractChapiter(event) {
    //console.log(event.type);

    const srcElement = event.srcElement;
    let chapitre = null;
    if (srcElement.className === "chapitre") {
        chapitre = srcElement;
    }
    else {
        const parents = getAllParents(srcElement);
        parents.forEach((element) => {
            //console.log("element", element.className);
            if (element.className === "chapitre") {
                chapitre = element;
            }
        });
    }

    //console.log("chapitre", chapitre);

    if (event.type === "click") {
        console.log("win half width :", window.innerWidth / 2, "chapitre.offsetLeft", chapitre.offsetLeft, chapitre);

        //select the content to show and put it in the right place on screen
        const contentIndex = chapitre.id.substring(chapitre.id.length - 1);
        const contentToShow = document.getElementById("content" + contentIndex);
        const tocImg = document.getElementById("toc-img");

        contentToShow.style.display = "block";
        tocImg.style.display = "block";

        //in the left or the right part ?
        if (chapitre.offsetLeft > (window.innerWidth / 2) - 10) {
            //right part
            console.log("right part");

            contentToShow.style.left = 0 + "px";
            contentToShow._positionName = "left";
            tocImg.style.left = window.innerWidth / 2 + "px";

            if (chapitre.offsetTop >= window.innerHeight / 2 - 10) {
                console.log("right bottom part");
                tocImg.style.top = 0 + "px";
                tocImg._positionName = "top-right";

            } else {
                console.log("right top part");
                tocImg.style.top = window.innerHeight / 2 + "px";
                tocImg._positionName = "bottom-right";
            }
        }
        else {
            //left part
            console.log("left part");
            contentToShow.style.display = "block";
            contentToShow.style.left = 0 + "px";
            contentToShow._positionName = "right";

            contentToShow.style.left = window.innerWidth / 2 + "px";
            tocImg.style.left = 0 + "px";

            if (chapitre.offsetTop >= window.innerHeight / 2 - 10) {
                console.log("left bottom part");
                tocImg.style.top = 0 + "px";
                tocImg._positionName = "top-left";
            } else {
                console.log("left top part");
                tocImg.style.top = window.innerHeight / 2 + "px";
                tocImg._positionName = "bottom-left";
            }
        }

    }

    //display management
    //show the main contents
    else if (event.type === "mouseenter") {
        //console.log("mouseenter");
        const intro = chapitre.getElementsByClassName("intro")[0];
        intro.style.opacity = 1;

    } else if (event.type === "mouseleave") {
        //console.log("mouseleave");
        const intro = chapitre.getElementsByClassName("intro")[0];
        intro.style.opacity = 0;
    }

}

function onInteractContent(event) {
    let srcElement = event.srcElement;
    //console.log("srcElement =", srcElement.tagName.toLowerCase());
    if (srcElement.tagName.toLowerCase() === "img") {
        srcElement = srcElement.parentElement;
    }

    const prevH2 = getPreviousSlibing(srcElement);
    const nextH2 = getNextSlibing(srcElement);
    //console.log("prevH2", prevH2, "nextH2", nextH2);
    let toMemory = null;
    if (prevH2.length > 0 && nextH2.length > 0) {
        toMemory = prevH2.concat(nextH2);
    }
    else {
        toMemory = prevH2;
    }

    //console.log("toMemory", toMemory);
    toMemory.forEach((el) => {
        elementsToPaginate.push(el);
    });

}

function onWindowResize() {

    const contents = [...document.getElementsByClassName("content")];
    contents.forEach((content) => {
        if (content._positionName === "left") {
            content.style.left = 0 + "px";
        }
        else if (content._positionName === "right") {
            content.style.left = window.innerWidth / 2 + "px";
        }
    });

    const tocImg = document.getElementById("toc-img");

    if (tocImg._positionName === "top-left") {
        tocImg.style.top = 0 + "px";
        tocImg.style.left = 0 + "px";
    }
    else if (tocImg._positionName === "bottom-left") {
        tocImg.style.top = window.innerHeight / 2 + "px";
        tocImg.style.left = 0 + "px";
    }
    else if (tocImg._positionName === "top-right") {
        tocImg.style.top = 0 + "px";
        tocImg.style.left = window.innerWidth / 2 + "px";
    }
    else if (tocImg._positionName === "bottom-right") {
        tocImg.style.top = window.innerHeight / 2 + "px";
        tocImg.style.left = window.innerWidth / 2 + "px";
    }



}

function getAllParents(element) {
    const parents = [];
    let parent = element.parentElement;

    while (parent) {
        parents.push(parent);
        parent = parent.parentElement;
    }

    return parents;
}

function getPreviousSlibing(element) {
    const prevSlibling = [];
    prevSlibling.push(element);

    let slibling = element.previousElementSibling;
    const tocImg = document.getElementById("toc-img");

    while (slibling) {
        //console.log("slibling", slibling);
        if (slibling.tagName === "H2") {
            prevSlibling.push(slibling);//get the H2
            //manage the transfer to toc-img div
            const h2Clone = slibling.cloneNode(true);
            console.log("h2Clone", h2Clone);
            const tocImgChildren = [...tocImg.children];

            let h2AlreadyThere = false;
            tocImgChildren.forEach((element) => {
                console.log(element.innerText, h2Clone.innerText);
                if (element.innerText === h2Clone.innerText) {
                    h2AlreadyThere = true;
                }
            });
            if (!h2AlreadyThere) {
                tocImg.appendChild(h2Clone);
            }

            prevSlibling.reverse();
            console.log("prevSlibling", prevSlibling);
            return prevSlibling;
        }
        else {
            prevSlibling.push(slibling);
        }
        slibling = slibling.previousElementSibling;
    }
    return prevSlibling;
}

function getNextSlibing(element) {
    const nextSlibling = [];
    //nextSlibling.push(element);
    let slibling = element.nextElementSibling;

    while (slibling) {

        if (slibling.tagName === "H2") {
            console.log("nextSlibling", nextSlibling);
            return nextSlibling;
        }
        else {
            nextSlibling.push(slibling);
        }
        slibling = slibling.nextElementSibling;
    }
    //beware of the end of the document!
    return nextSlibling;
}

//wait to have all the element loaded (module scripts can't be defered)
window.addEventListener("load", async (event) => {
    await layoutHTML();
});

//interaction
const printBt = document.querySelector('#printBt');

printBt.addEventListener("click", () => {
    paginate(elementsToPaginate, paginationStyleList);
});
