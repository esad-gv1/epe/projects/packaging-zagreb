# 1 <br> Unveiling the History of Packaging

:::{.part}
## Raw Materials

Packaging, the silent companion of our everyday lives, hides a fascinating story that stretches back thousands of years. From humble beginnings in nature to modern marvels of innovation, this odyssey through time reveals not only the evolution of packaging but also changes in human society and technological progress.{.intro-c}
:::

:::{.part}

![paper](/md/img/paper.jpg){.focus}

## Paper and paper products

One way to categorize packaging is to describe them as flexible, semi-flexible, or rigid.
Flexible packaging includes, for example, paper dog food bags, plastic bags for potatoes, and paper or plastic bags we use to carry purchased products from the store.
Semi-flexible packaging examples are cereal boxes, many other food products, small household items, and many toys.
For many non-food products, packaging is more rigidly formed, with packaging materials holding the product and its components in place. Forms of rigid packaging include crates, glass bottles, and metal cans.

Fabric and paper are the oldest forms of flexible packaging. The use of flexible packaging materials began with the Chinese, who used sheets of processed tree bark to wrap food as early as the **first or second century BC.**
Over the following centuries, the Chinese also developed and refined paper-making techniques. The knowledge of how to make paper gradually moved west across Asia into Europe. In 1310, papermaking was introduced in England. The technique reached America in Germantown, Pennsylvania, in 1690.
Paper is essentially a thin sheet of cellulose. Cellulose is a fibrous material obtained from plants. Early paper was made from derived cellulose fibers from linen, a plant that also provides fibers for making fabrics. As the demand for paper grew, old rags became sought after as a source of fibers.
In 1867, a process was discovered for obtaining **pure cellulose, or fibers, from wood pulp**. Since wood was a cheap and abundant source of fibers, this source of fibers quickly replaced fabric fibers as the primary source of paper. Today, almost all paper uses wood pulp as a source of cellulose fibers.

A significant step for the use of paper in packaging came with the development of paper bags.
Commercial paper bags were first produced in Bristol, England, in 1844. Shortly thereafter, in 1852, Francis Wolle invented a bag-making machine in the United States. Further progress in the 1870s included glued paper bags of a design still used today.
In 1905, machines for automatic, assembly-line production of printed paper bags were invented. With the development of the glued paper bag, more expensive cotton flour bags were replaced. But a stronger multi-layer paper bag for packaging larger quantities of products did not replace fabric ones until 1925, when a machine for sewing the ends of the bag was finally invented.
Another important use of paper in packaging came with the development of cardboard. **The first cardboard** - or cardboard box - was produced in England in 1817, much later than the Chinese invented cardboard.
The form of cardboard (corrugated board) based on corrugated paper appeared in the 1850s. Essentially, this form of cardboard is made from thin sheets of cardboard that are formed into a corrugated shape and then *"faced"* in a sandwich between two flat sheets of cardboard. The strength, lightness, and cheapness of this material make it very useful for shipping and storage.
However, replacing wooden crates with new alternatives also led to conflicts between manufacturers and railways. Yet, around 1910, after many lawsuits between manufacturers and railways, corrugated cardboard shipping boxes began to replace self-made wooden crates and boxes used for commerce. Today, cardboard boxes are used almost universally for shipping products.
As with many innovations, the development of boxes was accidental. Robert Gair was a manufacturer of paper bags during the 1870s. While printing orders for seed bags, he came up with the idea of a cardboard box for transport.

:::


:::{.part}

![glass](/md/img/glass.jpg){.focus}

## Glass

Although glassmaking began in 7000 BC, as a branch of pottery, it was first industrialized in Egypt in 1500 BC. Made from basic materials (limestone, soda, sand, and silicon dioxide) that were abundant.
All ingredients are simply melted together and cast while hot. From that early discovery, the mixing process and ingredients have changed very little, but casting techniques have advanced dramatically.
**By 1200 BC, glass was pressed into molds to make glasses and bowls**. When the blowpipe was invented by the Phoenicians in the 300th century BC, not only was production accelerated, but it also allowed for the production of round vessels. Colors were available from the beginning, but clear transparent glass was not discovered until the beginning of the Christian era. Over the next 1000 years, the process continued to expand, but slowly, throughout Europe.

The division mold, developed in the 17th and 18th centuries, was used for irregular shapes and decorations. The manufacturer's identification and product name could then be molded into the produced jar. As techniques further refined in the 18th and 19th centuries, the prices of glass containers continued to decrease. Owens invented the first automatic rotary bottle-making machine, patented in 1889. Suddenly, we come to glass containers of all shapes and sizes becoming economically attractive to consumers from the early 1900s to the late 1960s when glass containers dominated the liquid product market.

In modern bottle-making, the **machine automatically produces 20,000 bottles per day**. While other packaging products, such as metals and plastics, have gained popularity since the 1970s, glass packaging is generally reserved for high-value products.
As a type of *"rigid packaging,"* glass today has many uses. The great weight, fragility, and cost have reduced glass markets in favor of metals and plastics. Yet, for products that have a high level of quality and desire for protected taste or aroma, glass is an effective packaging material.

:::


:::{.part}

![metal](/md/img/metal.jpg){.focus}

## Metal

Ancient boxes and cups made of silver and gold were too valuable for common use. Metals did not become common packaging materials until the development of other metals, namely stronger alloys, the ability to make thinner plates, foils, and solutions to the problem of rust, for which coatings have been developed over time.
One of the *"new metals"* that enabled the use of metals in packaging was **tin**. Resistant to corrosion and ounce for ounce, its value is comparable to that of silver. However, it can be *"cast"* in very thin layers over cheaper metals, and this process made it economical for making containers.

The process of extracting tin was discovered in Bohemia in 1200 BC, and tin-coated iron cans were known in Bavaria as early as the 14th century. However, the coating process was kept secret until the 1600s. Thanks to the Duchess of Saxony, who stole the technique, it spread throughout Europe. After William Underwood brought the process to the United States via Boston, **steel replaced iron**, improving quality.
Safe preservation of food in metal containers was realized in France in the 1800s. In 1809, General Napoleon Bonaparte offered 12,000 francs to anyone who could preserve food for his army. Nicholas Appert, a Parisian chef and confectioner, determined that food sealed in tin cans and sterilized by cooking could be preserved for a long time.

A year later (1810), **Peter Durand** from Britain patented the **sealed cylindrical tin can**.
Since food was now safe in metal packaging, other products were available in metal boxes. In the 1830s, cookies and matches were sold in cans, and by 1866 the first printed metal boxes were made in the USA for Dr. Lyon's Tooth Powder.
Aluminum particles were first extracted from bauxite ores in 1825 but were sold at a high price of 545 dollars per pound (0.45 kg). As better processes were developed, which began in 1852, prices continued to drop until 1942 when the price of a kilogram of aluminum was 14 dollars.
Although commercial foils entered the market in 1910, the first containers made of aluminum foil were designed in the early 1950s, while the aluminum can appeared only in 1959.

The invention of cans also required the **invention of can openers!** Initially, a hammer and chisel were the only method of opening cans. Then, in 1866, a metal passage screw was developed. Nine years later (1875), the can opener was invented. By modernizing mechanisms and adding electricity in later years, it is improved. The can opener remained for more than 100 years the most efficient method of accessing the can's contents.
Foldable, soft metal tubes, known today as *"flexible packaging,"* were first used for artistic paints in 1841. Toothpaste was invented in the 1890s and began to appear in foldable metal tubes. But for food products, this packaging was not utilized until the 1960s. Later, aluminum was changed to plastic for such food products as sandwich pastes, cake icings, and pudding toppings.

:::


:::{.part}

![plastic](/md/img/plastic.jpg){.focus}

## Plastic 

**Plastic** is the newest packaging material compared to metal, glass, and paper. Although discovered in the 19th century, most plastics were reserved for military and wartime use. Plastic has become a very important material, and a great variety of plastics have been developed in the last 170 years.
Several materials were discovered in the nineteenth century: styrene in 1831, vinyl chloride in 1835, and celluloid in the late 1860s. However, none of these materials became practical for packaging until the twentieth century.
Styrene was first distilled from balsam trees in 1831, but early products were brittle and easily breakable. Germany improved the process in 1933, and by the 1950s, styrofoam was available worldwide.
Materials for insulation and cladding, styrofoam boxes, cups, and meat trays for the food industry became popular.
**Vinyl chloride**, discovered in 1835, allows further development of rubber chemistry. As packaging, molded bottles for deodorants, shower gels were introduced in 1947 and 1958.

Today, some water and vegetable oil containers are made of vinyl chloride.
Celluloid was invented during the American Civil War. Due to a lack of ivory, a US billiard ball manufacturer offered $10,000 for finding an alternative to ivory. New York engineer John Wesley Hyatt, with his brother Isaiah Smith Hyatt, experimented for several years before creating a new material. Patented in 1870, *"celluloid"* could not be molded but carved and shaped, just like ivory.
**Cellulose acetate** was first derived from wood pulp in 1900 and developed for photographic purposes in 1909. Although DuPont produced cellophane in New York in 1924, it was not commercially used for packaging until the late 1950s and early 1960s. In the meantime, polyethylene films were reserved for the military. In 1933, they were used to protect telephone cables in submarines, but later they were important materials for the world of wartime radar cables and packaging of medicine tablets for war.

Other cellophanes and transparent films have been refined as outer wraps that maintain their shapes when folded. Originally transparent, but such films can also be opaque, colored, or with embossed patterns.
One of the most commonly used plastics is **polyethylene terephthalate (PET)**. This material was initially used only for containers and later for beverage packaging that entered the market in 1977. By 1980, food and other hot-fill products like jams could also be packaged in PET.
Current packaging designs begin to include recyclable plastic.

:::

