# 2 <br> Packaging evolution in Croatia

:::{.part}
## A fascinating journey

The development of packaging in Croatia represents a fascinating journey from traditional forms to contemporary innovations that keep pace with global trends. This evolution is reflected in changes in the food, cosmetics, pharmaceuticals, and other sectors where packaging plays a crucial role in product protection, consumer information, and the creation of a recognizable brand identity. Over decades, packaging has become more than just a container; it has turned into a means of communication, an expression of identity, often reflecting societal, economic, and environmental trends.{.intro-c}

In Croatia, the history of packaging dates far back into the past, where traditional materials such as clay pots, wooden boxes, and fabrics were used for packaging and transporting various goods. These rudimentary forms of packaging reflected the needs of society at the time and the resources that were available.

*"The traditional clay pot is not just a food container, but also a guardian of tradition and identity of our people."* - Professor Anica Štampar, Ethnologist

With the advent of the Industrial Revolution, changes also began to reflect in the area of packaging. Materials such as glass, metal, and paper began to be used for mass production of packaging. Factories started to emerge across the country, and the standardization of processes allowed for the production of larger quantities of packaging at more competitive prices. This development enabled market expansion and greater accessibility of various products to consumers throughout Croatia.

*"The Industrial Revolution brought not only changes in production but also in how we experience products through packaging."* - Dr. Ivan Horvat, Historian

![ford](/md/img/Ford_assembly_line_-_1913.jpg)

In the 20th century, technological advancements brought numerous innovations in the field of packaging. **Plastic became the dominant material** due to its practicality, flexibility, and relatively low production cost. However, with growing awareness of environmental issues, there emerged a need for more sustainable alternatives. Thus, biodegradable materials, recycling options, and other environmentally friendly materials began to be developed and integrated into modern packaging production processes.

*"We are faced with the challenge of finding a balance between the practicality of plastic and environmental sustainability. In this, I see an opportunity for the development of innovative solutions."* - Ivan Novak, Head of the Research Department at a packaging company.
:::

:::{.part}
## Design and Brand Identity

Today, packaging has become a key element in creating a **brand's identity**. The design of packaging is often as important as the product itself, as it provides consumers with their first impression of the product and can motivate them to make a purchase. Brands strive to distinguish their products through attractive design, quality materials, and innovative solutions that provide additional value to consumers.
:::

:::{.part}
## Trends and Future

The future is expected to see further development in the field of packaging, especially towards more sustainable and environmentally friendly options. Brands will increasingly aim to reduce waste, use recycled materials, and innovate solutions that minimize the negative impact on the environment. Additionally, technological progress will continue to shape packaging, with the development of smart packaging that offers additional functionalities such as consumption tracking, product information, and more.

*"Packaging is the first contact consumers have with our product. We want that first impression to be memorable and to encourage the consumer to make a purchase."* - Ana Marić, Marketing Manager at a food company.
:::

:::{.part}
## Design

Design involves creating innovative, functional, and purposeful product innovations, focusing exclusively on new products. Design should not be directly put into the function of the prevailing style of aesthetic and fashion demands but, recognizing particularly its societal and human component, should primarily be put into the function of satisfying people. In conditions of a higher standard of living when lower consumer needs are met, their demands towards product properties become more critical and varied. To develop a suitable product, its properties, specifically **efficiency, usability, maintenance, aesthetic appearance, and cost-effectiveness**, need to be optimally related to the **needs, desires, tastes, and inclinations** of potential consumers that will maximize the product's utility values (Meler, M., 2002., 358. – 359.).
In general, as a boundary promotional activity, it must primarily meet the following requirements (Meler, M., 2002., 359.):

- Functional requirements (usability, purposefulness, utility),
- Aesthetic requirements (harmony, attractiveness, beauty, harmoniousness, proportionality),
- Economic requirements (from the aspect of production and consumption, or ultimately price).

The development of packaging design has long passed the point where physical protection is its only task. The protective function is still one of the main tasks of packaging, but it is just one of several conditions that a package must meet to be called a well-designed product. Modern packaging design aims to anticipate and solve problems and meet the needs of an entire lifecycle. Although designing packaging encompasses a range of solutions for all its functions and stages, simply put (over time), this lifecycle can be divided into two parts (http://dizajn.hr/blog/dizajn-ambalaze/, 10.07.2019.).

Packaging must be practical to use. Any analysis that is more than just scratching the surface indicates that packaging must be viewed as an investment, not a cost. Unfortunately, many manufacturers still view packaging design through the **initial price of design**, thereby not considering the possible **profit** achieved through appropriate packaging design of their product (http://dizajn.hr/blog/dizajn-ambalaze/, 10.07.2019.).
It is crucial to add value to packaging, such as easy opening, simple storage, recyclability, and similar, to increase the product's value. When introducing innovations to the packaging of some well-known brands, manufacturers should retain design elements key to brand recognition. In case of changing key elements, customers might have trouble finding and recognizing previously known products, which could lead to a decrease in sales. To avoid such problems, it is advisable for manufacturers to gradually change design elements. This way, consumers will have the opportunity to get used to the new packaging (Zekiri, Hasani, 2015., 232. – 240.).

Packaging design elements can be divided into **two groups**, which are visual elements and informative elements. Visual elements consist of color, photography, product illustration, typography, innovation, shape, and size of the product. **Informative elements** display information about the product and the technology used for packaging production. **Visual elements** influence the consumer's decision to purchase the product. Most consumers see packaging as part of the product, so visual elements are a key part of the decision. The packaging design must stand out from competitive products as manufacturers directly communicate with consumers (Silayoi, Speece, 2004., 628.). Photography or illustration on the packaging attracts consumer attention. It helps the buyer form an image of the product. The image consumers create about the product represents the buyer's opinion of the brand (Zekiri, Hasani, 2015., 232. – 240.). (excerpt from the final work of the author Iva Matijašić)
:::
