# Packaging in Croatia

## Part one \: Unveiling the History of Packaging


Packaging, the silent companion of our everyday lives, hides a fascinating story that stretches back thousands of years. From humble beginnings in nature to modern marvels of innovation, this odyssey through time reveals not only the evolution of packaging but also changes in human society and technological progress.

:::.break

## PAPER AND PAPER PRODUCTS


One way to categorize packaging is to describe them as flexible, semi-flexible, or rigid.
Flexible packaging includes, for example, paper dog food bags, plastic bags for potatoes, and paper or plastic bags we use to carry purchased products from the store.
Semi-flexible packaging examples are cereal boxes, many other food products, small household items, and many toys.
For many non-food products, packaging is more rigidly formed, with packaging materials holding the product and its components in place. Forms of rigid packaging include crates, glass bottles, and metal cans.
Fabric and paper are the oldest forms of flexible packaging. The use of flexible packaging materials began with the Chinese, who used sheets of processed tree bark to wrap food as early as the first or second century BC.
Over the following centuries, the Chinese also developed and refined paper-making techniques. The knowledge of how to make paper gradually moved west across Asia into Europe. In 1310, papermaking was introduced in England. The technique reached America in Germantown, Pennsylvania, in 1690.
Paper is essentially a thin sheet of cellulose. Cellulose is a fibrous material obtained from plants. Early paper was made from derived cellulose fibers from linen, a plant that also provides fibers for making fabrics. As the demand for paper grew, old rags became sought after as a source of fibers.
In 1867, a process was discovered for obtaining pure cellulose, or fibers, from wood pulp. Since wood was a cheap and abundant source of fibers, this source of fibers quickly replaced fabric fibers as the primary source of paper. Today, almost all paper uses wood pulp as a source of cellulose fibers.
A significant step for the use of paper in packaging came with the development of paper bags.
Commercial paper bags were first produced in Bristol, England, in 1844. Shortly thereafter, in 1852, Francis Wolle invented a bag-making machine in the United States. Further progress in the 1870s included glued paper bags of a design still used today.
In 1905, machines for automatic, assembly-line production of printed paper bags were invented. With the development of the glued paper bag, more expensive cotton flour bags were replaced. But a stronger multi-layer paper bag for packaging larger quantities of products did not replace fabric ones until 1925, when a machine for sewing the ends of the bag was finally invented.
Another important use of paper in packaging came with the development of cardboard. The first cardboard - or cardboard box - was produced in England in 1817, much later than the Chinese invented cardboard.
The form of cardboard (corrugated board) based on corrugated paper appeared in the 1850s. Essentially, this form of cardboard is made from thin sheets of cardboard that are formed into a corrugated shape and then *"faced"* in a sandwich between two flat sheets of cardboard. The strength, lightness, and cheapness of this material make it very useful for shipping and storage.
However, replacing wooden crates with new alternatives also led to conflicts between manufacturers and railways. Yet, around 1910, after many lawsuits between manufacturers and railways, corrugated cardboard shipping boxes began to replace self-made wooden crates and boxes used for commerce. Today, cardboard boxes are used almost universally for shipping products.
As with many innovations, the development of boxes was accidental. Robert Gair was a manufacturer of paper bags during the 1870s. While printing orders for seed bags, he came up with the idea of a cardboard box for transport.


## GLASS 


Although glassmaking began in 7000 BC, as a branch of pottery, it was first industrialized in Egypt in 1500 BC. Made from basic materials (limestone, soda, sand, and silicon dioxide) that were abundant.
All ingredients are simply melted together and cast while hot. From that early discovery, the mixing process and ingredients have changed very little, but casting techniques have advanced dramatically.
By 1200 BC, glass was pressed into molds to make glasses and bowls. When the blowpipe was invented by the Phoenicians in the 300th century BC, not only was production accelerated, but it also allowed for the production of round vessels. Colors were available from the beginning, but clear transparent glass was not discovered until the beginning of the Christian era. Over the next 1000 years, the process continued to expand, but slowly, throughout Europe.
The division mold, developed in the 17th and 18th centuries, was used for irregular shapes and decorations. The manufacturer's identification and product name could then be molded into the produced jar. As techniques further refined in the 18th and 19th centuries, the prices of glass containers continued to decrease. Owens invented the first automatic rotary bottle-making machine, patented in 1889. Suddenly, we come to glass containers of all shapes and sizes becoming economically attractive to consumers from the early 1900s to the late 1960s when glass containers dominated the liquid product market.
In modern bottle-making, the machine automatically produces 20,000 bottles per day. While other packaging products, such as metals and plastics, have gained popularity since the 1970s, glass packaging is generally reserved for high-value products.
As a type of *"rigid packaging,"* glass today has many uses. The great weight, fragility, and cost have reduced glass markets in favor of metals and plastics. Yet, for products that have a high level of quality and desire for protected taste or aroma, glass is an effective packaging material.
 

## METAL

Ancient boxes and cups made of silver and gold were too valuable for common use. Metals did not become common packaging materials until the development of other metals, namely stronger alloys, the ability to make thinner plates, foils, and solutions to the problem of rust, for which coatings have been developed over time.
One of the *"new metals"* that enabled the use of metals in packaging was tin. Resistant to corrosion and ounce for ounce, its value is comparable to that of silver. However, it can be "cast" in very thin layers over cheaper metals, and this process made it economical for making containers.
The process of extracting tin was discovered in Bohemia in 1200 BC, and tin-coated iron cans were known in Bavaria as early as the 14th century. However, the coating process was kept secret until the 1600s. Thanks to the Duchess of Saxony, who stole the technique, it spread throughout Europe. After William Underwood brought the process to the United States via Boston, steel replaced iron, improving quality.
Safe preservation of food in metal containers was realized in France in the 1800s. In 1809, General Napoleon Bonaparte offered 12,000 francs to anyone who could preserve food for his army. Nicholas Appert, a Parisian chef and confectioner, determined that food sealed in tin cans and sterilized by cooking could be preserved for a long time.
A year later (1810), Peter Durand from Britain patented the sealed cylindrical tin can.
Since food was now safe in metal packaging, other products were available in metal boxes. In the 1830s, cookies and matches were sold in cans, and by 1866 the first printed metal boxes were made in the USA for Dr. Lyon's Tooth Powder.
Aluminum particles were first extracted from bauxite ores in 1825 but were sold at a high price of 545 dollars per pound (0.45 kg). As better processes were developed, which began in 1852, prices continued to drop until 1942 when the price of a kilogram of aluminum was 14 dollars.
Although commercial foils entered the market in 1910, the first containers made of aluminum foil were designed in the early 1950s, while the aluminum can appeared only in 1959.
The invention of cans also required the invention of can openers! Initially, a hammer and chisel were the only method of opening cans. Then, in 1866, a metal passage screw was developed. Nine years later (1875), the can opener was invented. By modernizing mechanisms and adding electricity in later years, it is improved. The can opener remained for more than 100 years the most efficient method of accessing the can's contents.
Foldable, soft metal tubes, known today as "flexible packaging," were first used for artistic paints in 1841. Toothpaste was invented in the 1890s and began to appear in foldable metal tubes. But for food products, this packaging was not utilized until the 1960s. Later, aluminum was changed to plastic for such food products as sandwich pastes, cake icings, and pudding toppings.


## PLASTIC

Plastic is the newest packaging material compared to metal, glass, and paper. Although discovered in the 19th century, most plastics were reserved for military and wartime use. Plastic has become a very important material, and a great variety of plastics have been developed in the last 170 years.
Several materials were discovered in the nineteenth century: styrene in 1831, vinyl chloride in 1835, and celluloid in the late 1860s. However, none of these materials became practical for packaging until the twentieth century.
Styrene was first distilled from balsam trees in 1831, but early products were brittle and easily breakable. Germany improved the process in 1933, and by the 1950s, styrofoam was available worldwide.
Materials for insulation and cladding, styrofoam boxes, cups, and meat trays for the food industry became popular.
Vinyl chloride, discovered in 1835, allows further development of rubber chemistry. As packaging, molded bottles for deodorants, shower gels were introduced in 1947 and 1958.
Today, some water and vegetable oil containers are made of vinyl chloride.
Celluloid was invented during the American Civil War. Due to a lack of ivory, a US billiard ball manufacturer offered $10,000 for finding an alternative to ivory. New York engineer John Wesley Hyatt, with his brother Isaiah Smith Hyatt, experimented for several years before creating a new material. Patented in 1870, "celluloid" could not be molded but carved and shaped, just like ivory.
Cellulose acetate was first derived from wood pulp in 1900 and developed for photographic purposes in 1909. Although DuPont produced cellophane in New York in 1924, it was not commercially used for packaging until the late 1950s and early 1960s. In the meantime, polyethylene films were reserved for the military. In 1933, they were used to protect telephone cables in submarines, but later they were important materials for the world of wartime radar cables and packaging of medicine tablets for war.
Other cellophanes and transparent films have been refined as outer wraps that maintain their shapes when folded. Originally transparent, but such films can also be opaque, colored, or with embossed patterns.
One of the most commonly used plastics is polyethylene terephthalate (PET). This material was initially used only for containers and later for beverage packaging that entered the market in 1977. By 1980, food and other hot-fill products like jams could also be packaged in PET.
Current packaging designs begin to include recyclable plastic.


# Packaging evolution in Croatia

### Introduction

The development of packaging in Croatia represents a fascinating journey from traditional forms to contemporary innovations that keep pace with global trends. This evolution is reflected in changes in the food, cosmetics, pharmaceuticals, and other sectors where packaging plays a crucial role in product protection, consumer information, and the creation of a recognizable brand identity. Over decades, packaging has become more than just a container; it has turned into a means of communication, an expression of identity, often reflecting societal, economic, and environmental trends.

In Croatia, the history of packaging dates far back into the past, where traditional materials such as clay pots, wooden boxes, and fabrics were used for packaging and transporting various goods. These rudimentary forms of packaging reflected the needs of society at the time and the resources that were available.

*"The traditional clay pot is not just a food container, but also a guardian of tradition and identity of our people."* - Professor Anica Štampar, Ethnologist

With the advent of the Industrial Revolution, changes also began to reflect in the area of packaging. Materials such as glass, metal, and paper began to be used for mass production of packaging. Factories started to emerge across the country, and the standardization of processes allowed for the production of larger quantities of packaging at more competitive prices. This development enabled market expansion and greater accessibility of various products to consumers throughout Croatia.
"The Industrial Revolution brought not only changes in production but also in how we experience products through packaging." - Dr. Ivan Horvat, Historian

In the 20th century, technological advancements brought numerous innovations in the field of packaging. Plastic became the dominant material due to its practicality, flexibility, and relatively low production cost. However, with growing awareness of environmental issues, there emerged a need for more sustainable alternatives. Thus, biodegradable materials, recycling options, and other environmentally friendly materials began to be developed and integrated into modern packaging production processes.
"We are faced with the challenge of finding a balance between the practicality of plastic and environmental sustainability. In this, I see an opportunity for the development of innovative solutions." - Ivan Novak, Head of the Research Department at a packaging company.

## Design and Brand Identity

Today, packaging has become a key element in creating a brand's identity. The design of packaging is often as important as the product itself, as it provides consumers with their first impression of the product and can motivate them to make a purchase. Brands strive to distinguish their products through attractive design, quality materials, and innovative solutions that provide additional value to consumers.

## Trends and Future

The future is expected to see further development in the field of packaging, especially towards more sustainable and environmentally friendly options. Brands will increasingly aim to reduce waste, use recycled materials, and innovate solutions that minimize the negative impact on the environment. Additionally, technological progress will continue to shape packaging, with the development of smart packaging that offers additional functionalities such as consumption tracking, product information, and more.

"Packaging is the first contact consumers have with our product. We want that first impression to be memorable and to encourage the consumer to make a purchase." - Ana Marić, Marketing Manager at a food company.

## Design

Design involves creating innovative, functional, and purposeful product innovations, focusing exclusively on new products. Design should not be directly put into the function of the prevailing style of aesthetic and fashion demands but, recognizing particularly its societal and human component, should primarily be put into the function of satisfying people. In conditions of a higher standard of living when lower consumer needs are met, their demands towards product properties become more critical and varied. To develop a suitable product, its properties, specifically efficiency, usability, maintenance, aesthetic appearance, and cost-effectiveness, need to be optimally related to the needs, desires, tastes, and inclinations of potential consumers that will maximize the product's utility values (Meler, M., 2002., 358. – 359.).
In general, as a boundary promotional activity, it must primarily meet the following requirements (Meler, M., 2002., 359.):

- Functional requirements (usability, purposefulness, utility),
- Aesthetic requirements (harmony, attractiveness, beauty, harmoniousness, proportionality),
- Economic requirements (from the aspect of production and consumption, or ultimately price).

The development of packaging design has long passed the point where physical protection is its only task. The protective function is still one of the main tasks of packaging, but it is just one of several conditions that a package must meet to be called a well-designed product. Modern packaging design aims to anticipate and solve problems and meet the needs of an entire lifecycle. Although designing packaging encompasses a range of solutions for all its functions and stages, simply put (over time), this lifecycle can be divided into two parts (http://dizajn.hr/blog/dizajn-ambalaze/, 10.07.2019.).
Packaging must be practical to use. Any analysis that is more than just scratching the surface indicates that packaging must be viewed as an investment, not a cost. Unfortunately, many manufacturers still view packaging design through the initial price of design, thereby not considering the possible profit achieved through appropriate packaging design of their product (http://dizajn.hr/blog/dizajn-ambalaze/, 10.07.2019.).
It is crucial to add value to packaging, such as easy opening, simple storage, recyclability, and similar, to increase the product's value. When introducing innovations to the packaging of some well-known brands, manufacturers should retain design elements key to brand recognition. In case of changing key elements, customers might have trouble finding and recognizing previously known products, which could lead to a decrease in sales. To avoid such problems, it is advisable for manufacturers to gradually change design elements. This way, consumers will have the opportunity to get used to the new packaging (Zekiri, Hasani, 2015., 232. – 240.).
Packaging design elements can be divided into two groups, which are visual elements and informative elements. Visual elements consist of color, photography, product illustration, typography, innovation, shape, and size of the product. Informative elements display information about the product and the technology used for packaging production. Visual elements influence the consumer's decision to purchase the product. Most consumers see packaging as part of the product, so visual elements are a key part of the decision. The packaging design must stand out from competitive products as manufacturers directly communicate with consumers (Silayoi, Speece, 2004., 628.). Photography or illustration on the packaging attracts consumer attention. It helps the buyer form an image of the product. The image consumers create about the product represents the buyer's opinion of the brand (Zekiri, Hasani, 2015., 232. – 240.). (excerpt from the final work of the author Iva Matijašić)

# Unveiling Croatian Canvas: A Look at Packaging Design

## Introduction

Croatia, a country intertwined with rich culture and industry, boasts a unique landscape in packaging design. From the raw beauty of the Adriatic coast to the fertile plains of Slavonia, Croatian packaging reflects a blend of tradition and contemporary innovation. This text delves into the essence of Croatian packaging design, exploring its distinctive characteristics, the play of local influences, and a promising future.

## The Charm of Local Identity

Croatian packaging design heavily relies on its cultural heritage. Vivid colors reminiscent of the sun-soaked Mediterranean coast and the azure Adriatic Sea are a common theme. Motifs inspired by traditional patterns, such as the "pleter," and historical architecture add a touch of local charm.
Sustainability is an increasing concern, and Croatian packaging design embraces eco-friendly practices. Producers are increasingly opting for recycled materials like cardboard and paper, with minimal use of plastic. Biodegradable coatings and inks are becoming more prevalent, reflecting a commitment to environmental protection.

## The Influence of Global Trends

The landscape of Croatian packaging design is not an isolated phenomenon. World trends of minimalism, functionality, and user experience leave their mark. Clean lines, bold typography, and a focus on user-friendliness are evident in contemporary Croatian packaging.

## Looking into the Future

Croatian packaging design is poised for continued growth. As local designers collaborate with international partners and embrace technological advancements, the future promises exciting discoveries. 3D printing for customized packaging and the use of augmented reality for interactive experiences are potential areas of exploration.

# Eco-Friendly Packaging as Part of a Marketing Strategy

## Green Marketing

Marketing is omnipresent, and today we witness its prevalence across all segments of social life. According to Kotler, Keller, and Martinović (2014), marketing is concerned with identifying and meeting human and societal needs. Meler (1999) states that the official definition of marketing, according to The American Marketing Association (AMA, 1985), is as follows: "Marketing is the process of planning and executing the conception, pricing, promotion, and distribution of ideas, goods, and services to create exchanges that satisfy individual and organizational goals." Effective marketing management is crucial to attract new consumers and retain existing ones, delivering superior value. Moreover, companies must continually invest in product and service innovations and consider their consumers' opinions to tailor their marketing strategies accordingly. Regular refreshing of strategies is essential, rather than relying on outdated practices. Dujak and Ham (2008) mention that the first definition of green marketing comes from The American Marketing Association (AMA, 1975), defining it as the study of the positive and negative aspects of marketing activities on pollution, energy depletion, and the depletion of non-energy resources. Furthermore, they highlight Stanton and Futrell's 1987 definition, which describes green marketing as a set of activities designed to create and facilitate any exchange intended to satisfy human needs or desires, in a way that the satisfaction of these needs and desires causes minimal negative impact on the natural environment.

## Employing Eco-Friendly Packaging through Corporate Marketing

Eco-friendly packaging typically has a lesser negative impact on the environment, made from recycled materials and can be recycled after use. Additionally, eco-packaging can offer a competitive edge as it not only protects the natural environment but also saves money on energy consumption, equipment procurement, support, and management.

## Targeting Specific Consumer Groups

Environmental protection issues pose one of the greatest challenges humanity has ever faced. Climate change, the depletion of our natural resources, and the degradation of our natural environment are very real concerns that have led to a new group of people, who can be called green consumers.

## Importance of Using Eco-Friendly Packaging in Business

Today, through various media, we see a changing global awareness of the importance of environmental protection, and companies are under pressure to act socially responsible. Dujak and Ham (2008) cite the so-called 3 R's formula of green marketing. According to this formula, a company can make a significant contribution to environmental protection through three steps: reduce, reuse, and recycle. (Chapter from the final thesis of author Martina Varga).

# Conclusion

Croatian packaging has undergone a fascinating evolution, from rudimentary beginnings to contemporary innovations. The design reflects not just technological advancement but also cultural identity, environmental concern, and marketing trends. The fusion of tradition and innovation, focus on sustainability, and integration of global trends with local charm make Croatian packaging distinctive and dynamic. The future promises exciting possibilities towards interactivity and sustainability, confirming that packaging is more than just a wrapper – it's a story about culture, creativity, and care for the planet.
